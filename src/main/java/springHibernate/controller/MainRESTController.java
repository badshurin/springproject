package springHibernate.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import springHibernate.dao.PostDaoImpl;
import springHibernate.dao.PostsEntity;
import springHibernate.model.PostResponse;

import java.util.List;

@RestController()
public class MainRESTController {

    @Autowired
    private PostDaoImpl postDao;
    @Autowired
    private ObjectMapper mapper;

    @RequestMapping("/welcome")
//    @ResponseBody
    public String welcome() {
        return "Welcome to RestTemplate Example";
    }

    // URL:
    // http://localhost:8080/login/post
    // http://localhost:8080/login/post.xml
    // http://localhost:8080/login/post.json

    @RequestMapping(value = "/post",
            method = RequestMethod.GET,
            produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public //@ResponseBody
    List<PostsEntity> findAllPost1() {
        return postDao.findAllPost();
    }

    @RequestMapping(value = "/postid/{empNo}", //
                method = RequestMethod.GET, //
                produces = {MediaType.APPLICATION_JSON_VALUE })
//        @ResponseBody
        public PostsEntity getPostId(@PathVariable("empNo") int postNo) {
            return postDao.findById(postNo);
    }

    @RequestMapping(value = "/postname/{empNo}", //
            method = RequestMethod.GET, //
            produces = {MediaType.APPLICATION_JSON_VALUE })
//    @ResponseBody
    public PostsEntity getPostName(@PathVariable("empNo") String postNo) {
        return postDao.byName(postNo);
    }

    @RequestMapping(value = "/postadd",
    method = RequestMethod.POST,
    produces = MediaType.APPLICATION_JSON_VALUE)
    public PostsEntity addPost(@RequestBody PostsEntity user){
        postDao.addPost(user);
        return user;
    }
}
