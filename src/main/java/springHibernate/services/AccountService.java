package springHibernate.services;

public interface AccountService {
    int testData(String name, String password);
    boolean netPost(String name, String password);
}
