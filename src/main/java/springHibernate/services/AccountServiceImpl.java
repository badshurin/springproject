package springHibernate.services;

import springHibernate.dao.PostsEntity;

public class AccountServiceImpl implements AccountService{

    @Override
    public int testData(String name, String password) {
        UserService userService = new UserServiceImpl();
        PostsEntity usern;
        try{
            usern = userService.userByName(name);
        } catch (Exception e) {
            usern = null;
        }
        if (usern != null) {return -1;}
        else {
            if (name.length()>2 && password.length()>2) {
                PostsEntity newUser = new PostsEntity(name,password);
                userService.addUser(newUser);
                return 1;
            }
            else return 0;
        }
    }

    @Override
    public boolean netPost(String name, String password) {
        UserService userService = new UserServiceImpl();
        PostsEntity user;
        try{
            user = userService.userByName(name);
        } catch (Exception e) {
            user = null;
        }
        if (user != null && user.getPassword().equals(password)) {return true;}
        else return false;

    }
}
