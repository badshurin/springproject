package springHibernate.services;
import springHibernate.dao.PostDaoImpl;
import springHibernate.dao.PostsEntity;

import java.util.List;

public class UserServiceImpl implements UserService {
    private PostDaoImpl postDao = new PostDaoImpl();
    public void setPostDao(PostDaoImpl postDao){
        this.postDao = postDao;
    }

    @Override
    public PostsEntity userByName(String name) throws Exception {
        return this.postDao.byName(name);
    }

    @Override
    public void addUser(PostsEntity user) {
        this.postDao.addPost(user);
    }

    @Override
    public List<PostsEntity> findAllUser() {
        return this.postDao.findAllPost();
    }

}
